package com.mastercard.blockchain;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * POJO class representing a Post stored in the Firebase Database.
 */
// [START post_class]
@IgnoreExtraProperties
public class Account {

    public String address;
    public long balance;
    public long kwh;
    public long last_update;
    public Map<String, Boolean> stars = new HashMap<String, Boolean>();

    public Account() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Account(String address, long balance, long kwh, long last_update) {
        this.address = address;
        this.balance = balance;
        this.kwh = kwh;
        this.last_update = last_update;
    }
}
// [END post_class]
