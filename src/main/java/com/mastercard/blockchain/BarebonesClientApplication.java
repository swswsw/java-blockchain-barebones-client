package com.mastercard.blockchain;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mastercard.api.blockchain.App;
import com.mastercard.api.blockchain.Block;
import com.mastercard.api.blockchain.TransactionEntry;
import com.mastercard.api.core.ApiConfig;
import com.mastercard.api.core.exception.ApiException;
import com.mastercard.api.core.model.RequestMap;
import com.mastercard.api.core.model.ResourceList;
import com.mastercard.api.core.security.oauth.OAuthAuthentication;
import org.apache.commons.cli.*;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Copyright (c) 2017 Mastercard. All Rights Reserved.
 *
 */
public class BarebonesClientApplication {

    private String ENCODING = "base64";
    private String APP_ID = getAppIdFromProtoBuffer();
    private String appId ;

    private String lastHash = "";
    private String lastSlot = "";

    public static void main(String ... args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        Options options = createOptions();
        CommandLine cmd = parser.parse(options, args);
        new BarebonesClientApplication().start(cmd, options);
    }


    private void start(CommandLine cmd, Options options) throws FileNotFoundException {
        System.out.println(readResourceToString("/help.txt"));
        System.out.println();
        initApi(cmd);
        
        doFirebase();
        
//        while (true) {
//        	try {
//        		Thread.sleep(1000);
//        	} catch (InterruptedException ex) {
//        		ex.printStackTrace();
//        	}
//        }
        
        // comment out if we don't want see interactive stuff.
        menu(cmd, options);
    }

    private void menu(CommandLine cmd, Options options) {
        final String quit = "0";
        String option = "";
        while(!option.equals(quit)) {
            printHeading("MENU");
            System.out.println("1. Create entry");
            System.out.println("2. Retrieve entry");
            System.out.println("3. Retrieve block");
            System.out.println("4. Retrieve last confirmed block");
            System.out.println("5. Show Protocol Buffer Definition");
            System.out.println("6. Re-initialize API");
            System.out.println("7. Print Command Line Options");
            System.out.println(quit + ". Quit");
            option = captureInput("Option", quit);
            if(option.equals("1")) {
                createEntry();
            }
            else if(option.equals("2")) {
                retrieveEntry();
            }
            else if(option.equals("3")) {
                retrieveBlock();
            }
            else if(option.equals("4")) {
                retrieveLastConfirmedBlock();
            }
            else if(option.equals("5")) {
                printHeading("SHOW PROTOCOL BUFFER");
                System.out.println(readResourceToString("/message.proto"));
                captureInput("(press return to continue)", null);
            }
            else if(option.equals("6")) {
                try {
                    printHeading("INITIALIZE API");
                    initApi(cmd);
                }
                catch(FileNotFoundException e) {
                    System.err.println(e.getMessage());
                }
                captureInput("(press return to continue)", null);
            }
            else if(option.equals("7")) {
                printHeading("COMMAND LINE OPTIONS");
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("java -jar <jarfile>", options);
                captureInput("(press return to continue)", null);
            }
            else if(option.equals(quit)) {
                System.out.println("Goodbye");
            }
            else {
                System.out.println("Unrecognised option");
            }
        }
    }

    private void createEntry() {
        printHeading("CREATE ENTRY");
        String message = captureInput("Entry Text", "Hello Blockchain!");
        try {
            Protobuf.Message.Builder builder = Protobuf.Message.newBuilder();
            Protobuf.Message protocolBuffer = builder.setText(message).build();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            protocolBuffer.writeTo(baos);
            String encoded = encode(baos.toByteArray(), ENCODING);
            RequestMap request = new RequestMap();
            request.put("app", appId);
            request.put("encoding", ENCODING);
            request.put("value", encode(baos.toByteArray(), encoded));
            TransactionEntry response = TransactionEntry.create(request);
            lastHash = response.get("hash").toString();
            lastSlot = response.get("slot").toString();
            printEntry(response);
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
        catch (ApiException e) {
            System.err.println(e.getMessage());
        }
        captureInput("(press return to continue)", null);
    }

    private String updateNode(String file, String appId) {
        printHeading("Updating node with new protocol buffer...");
        try {
            RequestMap map = new RequestMap();
            map.set("id", appId);
            map.set("name", appId);
            map.set("description", "");
            map.set("version", 0);
            map.set("definition.format", "proto3");
            map.set("definition.encoding", "base64");
            map.set("definition.messages",
                    Base64.getEncoder().encodeToString(readResourceToString(file).replace(APP_ID, appId).getBytes()));
            new App(map).update();
            System.out.println("Node updated");
            App app = App.read(appId);
            JSONObject definition = (JSONObject) app.get("definition");
            System.out.println("New Format: " + definition.get("format"));
            System.out.println("New Encoding: " + definition.get("encoding"));
            System.out.println("New Messages: " + definition.get("messages"));
        } catch (ApiException e) {
            System.err.println("API Exception " + e.getMessage());
        }
        return appId;
    }

    private void retrieveEntry() {
        printHeading("RETRIEVE HEADING");
        String hash = captureInput("Entry Hash", lastHash);
        try {
            RequestMap request = new RequestMap();
            request.put("hash", hash);
            TransactionEntry response = TransactionEntry.read("", request);
            printEntry(response);
        }
        catch(ApiException e) {
            System.err.println(e.getMessage());
        }
        captureInput("(press return to continue)", null);
    }

    private void printEntry(TransactionEntry response) {
        System.out.println("Hash: " + response.get("hash").toString());
        System.out.println("Slot: " + response.get("slot").toString());
        System.out.println("Status: " + response.get("status").toString());
        if(response.containsKey("value")) {
            String hexEncoded = response.get("value").toString();
            System.out.println("Value: " + hexEncoded);
            try {
                Protobuf.Message message = Protobuf.Message.parseFrom(decode(hexEncoded, "hex"));
                System.out.println("Decoded Value: " + message.getText());
            }
            catch(DecoderException | InvalidProtocolBufferException e) {
                System.err.println("Unable to decode: " + hexEncoded);
            }
        }
    }

    private void retrieveBlock() {
        printHeading("RETRIEVE BLOCK");
        String slot = captureInput("Block Id", lastSlot);
        try {
            RequestMap request = new RequestMap();
            request.put("id", slot);
            Block response = Block.read(slot);
            printBlock(response);
        }
        catch(ApiException e) {
            if(e.getHttpStatus() == 404) {
                System.err.println("Block not found (it may not have been written yet!)");
            }
            else {
                System.err.println(e.getMessage());
            }
        }
        captureInput("(press return to continue)", null);
    }

    private void retrieveLastConfirmedBlock() {
        printHeading("RETRIEVE LAST BLOCK");
        try {
            ResourceList<Block> response = Block.list();
            if(response.getList().size() > 0) {
                printBlock(response.getList().get(0));
            }
            else {
                System.err.println("No confirmed blocks returned!");
            }
        }
        catch(ApiException e) {
            if(e.getHttpStatus() == 404) {
                System.err.println("Block not found (it may not have been written yet!)");
            }
            else {
                System.err.println(e.getMessage());
            }
        }
        captureInput("(press return to continue)", null);
    }

    private void printBlock(Block response) {
        System.out.println("Hash: " + response.get("hash").toString());
        System.out.println("Slot: " + response.get("slot").toString());
        System.out.println("Version: " + response.get("version").toString());
        System.out.println("Previous Block: " + response.get("previous_block").toString());
        System.out.println("Nonce: " + response.get("nonce").toString());
        System.out.println("Authority: " + response.get("authority").toString());
        System.out.println("Signature: " + response.get("signature").toString());
        JSONArray partitions = (JSONArray) response.get("partitions");
        if(partitions.size() > 0) {
            for (int i = 0; i < partitions.size(); i++) {
                System.out.println("Partition[" + i + "]: ");
                JSONObject o = JSONObject.class.cast(partitions.get(i));
                System.out.println("\tApplication: " + o.get("application"));
                System.out.println("\tMerkle Root: " + o.get("merkle_root"));
                System.out.println("\tEntry Count: " + o.get("entry_count"));
                JSONArray entries = (JSONArray) o.get("entries");
                if(entries.size() > 0) {
                    for (int j = 0; j < entries.size(); j++) {
                        String entry = String.class.cast(entries.get(j));
                        System.out.println("\t\tEntries[" + j + "]: " + entry);
                    }
                }
                else {
                    System.out.println("Entries: NONE");
                }
            }
        }
        else {
            System.out.println("Partitions: NONE");
        }
    }

    private void initApi(CommandLine cmd) throws FileNotFoundException {
        String keystorePath = captureInputFile("Keystore", cmd.getOptionValue("keystorePath", ""));
        String storePass = captureInput("Keystore Password", cmd.getOptionValue("storePass", "keystorepassword"));
        String consumerKey = captureInput("Consumer Key", cmd.getOptionValue("consumerKey", ""));
        String keyAlias = captureInput("Key Alias", cmd.getOptionValue("keyAlias", "keyalias"));
        appId = captureInput("App Id (Team name e.g. TM07):", "TRIA");
        ApiConfig.setAuthentication(
                new OAuthAuthentication(
                        consumerKey,
                        new FileInputStream(keystorePath),
                        keyAlias,
                        storePass));
        ApiConfig.setDebug(cmd.hasOption("verbosity"));
        ApiConfig.setSandbox(true);
        updateNode("/message.proto", appId);
    }

    private String captureInputFile(String question, String defaultAnswer) {
        boolean noFile = true;
        String keystorePath = null;
        while(noFile) {
            keystorePath = captureInput(question, defaultAnswer);
            keystorePath = keystorePath.replaceFirst("^~/", System.getProperty("user.home") + "/");
            if (Files.notExists(Paths.get(keystorePath))) {
                System.out.println("File Not Found");
            } else {
                noFile = false;
            }
        }
        return keystorePath;
    }

    private String captureInput(String question, String defaultAnswer) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        if(defaultAnswer == null) {
            System.out.print(question + ": ");
        }
        else {
            System.out.print(question + " [" + defaultAnswer + "]: ");
        }
        String s;
        try {
            s = br.readLine();
            if (s == null || "".equals(s)) {
                s = defaultAnswer;
            }
        }
        catch(IOException e) {
            s = defaultAnswer;
        }
        return s;
    }

    private void printHeading(String heading) {
        System.out.println("============ " + heading +" ============");
    }

    private static String readResourceToString(String path) {
        return new BufferedReader(new InputStreamReader(BarebonesClientApplication.class.getResourceAsStream(path)))
                .lines().collect(Collectors.joining("\n"));
    }

    private String encode(byte[] bytes, String encoding) {
        if(encoding.equals("hex")) {
            return Hex.encodeHexString(bytes);
        }
        else {
            return Base64.getEncoder().encodeToString(bytes);
        }
    }

    private byte[] decode(String encoded, String encoding) throws DecoderException {
        if(encoding.equals("hex")) {
            return Hex.decodeHex(encoded.toCharArray());
        }
        else {
            return Base64.getDecoder().decode(encoded);
        }
    }

    public static String getAppIdFromProtoBuffer() {
        String protoBuf = readResourceToString("/message.proto");
        Pattern pattern = Pattern.compile("package\\s(.[A-Za-z0-9]+);", Pattern.MULTILINE);
        Matcher m = pattern.matcher(protoBuf);
        if(m.find()) {
            return m.group(1);
        }
        return "";
    }

    private static Options createOptions() {
        Options options = new Options();

        options.addOption("ck", "consumerKey", true, "consumer key (mastercard developers)");
        options.addOption("kp","keystorePath", true, "the path to your keystore (mastercard developers)");
        options.addOption("ka","keyAlias", true, "key alias (mastercard developers)");
        options.addOption("sp","storePass", true, "keystore password (mastercard developers)");
        options.addOption("v","verbosity", false, "log mastercard developers sdk to console");

        return options;
    }

    
    private void initFirebase() {
    	try {
	    	// i use firebase database that requires no credential, but firebase options
	    	FileInputStream serviceAccount = new FileInputStream("src/main/resources/serviceAccountKey.json");
	
	    	FirebaseOptions options = new FirebaseOptions.Builder()
	    	  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
	    	  .setDatabaseUrl("https://powercoin2020.firebaseio.com/")
	    	  .build();
	
	    	FirebaseApp.initializeApp(options);
    	} catch (FileNotFoundException ex) {
    		ex.printStackTrace();
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	}
    }
    
    private void doFirebase() {
    	initFirebase();
    	
    	// listen to database change
    	// doesn't seem to work due to credential
    	DatabaseReference ref = FirebaseDatabase
    		    .getInstance()
    		    .getReference("accounts/");
    		ref.addChildEventListener(new ChildEventListener() {
    		    @Override
    		    public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
    		    	Object document = dataSnapshot.getValue();
    		        System.out.println(document);
    		        Account newAccount = dataSnapshot.getValue(Account.class);
    		        System.out.println("address: " + newAccount.address);
    		        System.out.println("balance: " + newAccount.balance);
    		        System.out.println("kwh: " + newAccount.kwh);
    		        System.out.println("last_update: " + newAccount.last_update);
    		        createAccount(newAccount);
//    		        Post newPost = dataSnapshot.getValue(Post.class);
//    		        System.out.println("Author: " + newPost.author);
//    		        System.out.println("Title: " + newPost.title);
//    		        System.out.println("Previous Post ID: " + prevChildKey);
    		    }

    		    @Override
    		    public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {}

    		    @Override
    		    public void onChildRemoved(DataSnapshot dataSnapshot) {}

    		    @Override
    		    public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {}

    		    @Override
    		    public void onCancelled(DatabaseError databaseError) {}
    		});
    		
    		// this is one time only
//    		ref.addListenerForSingleValueEvent(new ValueEventListener() {
//    		    @Override
//    		    public void onDataChange(DataSnapshot dataSnapshot) {
//    		        Object document = dataSnapshot.getValue();
//    		        System.out.println(document);
//    		    }
//    		    
//    		    @Override
//    		    public void onCancelled(DatabaseError err) {
//    		    	System.err.println(err);
//    		    }
//    		});
    }
    
    private void createAccount(Account account) {
        printHeading("CREATE account");
        //String message = captureInput("Entry Text", "Hello Blockchain!");
        try {
            Protobuf.Account.Builder builder = Protobuf.Account.newBuilder();
            Protobuf.Account protocolBuffer = builder.setAddress(account.address)
            		.setBalance(account.balance)
            		.setKwh(account.kwh)
            		.setLastUpdate(account.last_update)
            		.build();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            protocolBuffer.writeTo(baos);
            String encoded = encode(baos.toByteArray(), ENCODING);
            RequestMap request = new RequestMap();
            request.put("app", appId);
            request.put("encoding", ENCODING);
            request.put("value", encode(baos.toByteArray(), encoded));
            TransactionEntry response = TransactionEntry.create(request);
            lastHash = response.get("hash").toString();
            lastSlot = response.get("slot").toString();
            printEntry(response);
        }
        catch(IOException e) {
            System.err.println(e.getMessage());
        }
        catch (ApiException e) {
            System.err.println(e.getMessage());
        }
        //captureInput("(press return to continue)", null);
    }
}
